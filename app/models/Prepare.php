<?php

class Prepare {
    public static $conditions = [
            "{#name}" => "ho_ten",
        ];

    public static function transform($template, $data){
        //dd(self::$conditions["{#name}"]);
        $result = str_replace('\n', '<br>', $template);
        $conditionRegex = '/{#\w+}/';
        preg_match_all($conditionRegex, $template, $matches);
        if ($matches) {
            foreach($matches[0] as $condition) {
                if (isset(self::$conditions[$condition])) {
                    $result = str_replace($condition , $data->{self::$conditions[$condition]}, $result);
                }
            }
        }
        return $result;
    }
}
