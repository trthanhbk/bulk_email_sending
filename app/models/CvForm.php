<?php
use Laracasts\Validation\FormValidator;
class CvForm extends FormValidator {
   protected $rules = [
        'title' => 'required',
        'body' => 'required',
        'cv' => 'required|mimes:xlsx',
        'cc'=> 'mail_array'
    ];
}
