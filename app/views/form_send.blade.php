<!DOCTYPE html>
<html lang="vi">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <title>Bulk Email Sending</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
        <style>
            .error {
                color: red;
            }
            .msg {
                background-color: yellow;
                color: blue;
            }
            .wrapper{ width: 500px; margin: 30px auto 0; background: #eee; padding: 50px 30px 30px;}
            .wrapper .form-body input,
            .wrapper .form-body textarea{ width: 100%; }
            .wrapper .submit-form{ text-align: right;}
            .wrapper .submit-form input {background: none; border: none;}
            .wrapper .submit-form input[type='submit']:focus{ outline: none;}
            .wrapper ul{margin: 0 0 25px;padding: 0 0 0 15px;}
            .wrapper ul li{margin: 0;padding: 0;list-style: none;}

        </style>
    </head>
    <body>
    <div class="wrapper">
        @if(Session::has('message'))
        <div class="msg">
        {{ Session::get('message') }}
        </div>
        @endif

        @if(Session::has('error'))
            <?php $error_msg = Session::get('error') ; ?>
            @if($error_msg)
                <?php $error_arr = explode(',', $error_msg) ; ?>
                <div class="msg">                
                But still have some email(s) is(are) not valid:
                    <ul>
                        @foreach($error_arr as $email_invalid)
                            <li>{{ $email_invalid }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        @endif

        Valid template:
        <ul>
            @foreach($conditions as $condition)
                <li>* {{{ $condition }}}</li>
            @endforeach
        </ul>
        {{ Form::open(['action' => 'HomeController@sendMail', 'files' => true]) }}
        <div class='form-body'>
            <div class="form-group">
                <label>CC: </label>
                {{ Form::text('cc') }}
                @include('field_errors', ['field' => 'cc'])
            </div>
            <div class="form-group">
                <label>Title: </label>
                {{ Form::text('title') }}
                @include('field_errors', ['field' => 'title'])
            </div> 
            <div class="form-group">
                <label >Body: </label>
                {{ Form::textarea('body') }}
                @include('field_errors', ['field' => 'body'])
            </div>
            <div class="form-group">
                <label>CV: </label>
                {{ Form::file('cv') }}
                @include('field_errors', ['field' => 'cv'])
            </div>            
        </div>

            <div class="submit-form"><div class="btn btn-default">{{ Form::submit('Submit') }} </div></div>

        {{ Form::close() }}
    </div>

    </body>
</html>
