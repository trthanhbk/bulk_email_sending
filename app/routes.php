<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function(){
    list($conditions, $replacement) = array_divide(Prepare::$conditions);
    return View::make('form_send', compact("conditions"));
});

Route::post('/process', 'HomeController@sendMail');
